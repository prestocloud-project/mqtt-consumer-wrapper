# MQTT MESSAGE CONSUMER WRAPPER

Mqtt consumer wrapper is a library that uses [`eclipse.paho.client.mqttv3` version `1.2.2`](https://mvnrepository.com/artifact/org.eclipse.paho/org.eclipse.paho.client.mqttv3/1.2.2) and adds a layer for easier connectivity with the PrEsto Communication and Messaging Broker.

Libraries are available on Nissatech public Maven, and in order to use them you can:

- #### Add the repository and dependency tag to your pom.xml file:
```xml
<repository>
    <id>archiva.public</id>
    <name>Nissatech Public Repository</name>
    <url>https://maven.nissatech.com/repository/public/</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```
```xml
<dependency>
    <groupId>com.nissatech.presto</groupId>
    <artifactId>mqtt-consumer</artifactId>
    <version>4.0</version>
</dependency>
```
- #### Download jar and include it in your project:

https://maven.nissatech.com/repository/public/com/nissatech/presto/mqtt-consumer/4.0/mqtt-consumer-4.0-jar-with-dependencies.jar

## Basic usage
```java
import consumer.MqttConsumer;
import exceptions.AlreadySubscribedException;
import exceptions.SubscribingFailedException;
import exceptions.NoHostException;
import java.io.IOException;

public class Receive_MQTT {
    public static void main(String argv[]) {
        String topic = "mqtt/#";
        java.util.function.BiConsumer<String, String> consumerFunction = Receive_MQTT::doNothing;
        MqttConsumer consumer = new MqttConsumer("3.120.91.124", false, consumerFunction);
        //consumer.setUsernameAndPassword("nissatech","**********");
        //consumer.useSSL("3.120.91.124", "**********");
        boolean p = true;
        while (p) {
            try {
                consumer.subscribe(topic);
                p = false;
            } catch (NoHostException | IOException | AlreadySubscribedException e1) {
                e1.printStackTrace();
                p = false;
            } catch (SubscribingFailedException e) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private static void doNothing(String message, String topic) {}
}
```
## Constructors

`MqttConsumer(String host_or_dns_address, boolean is_DNS_address, String topic, BiConsumer<String, String> consumerFunction)` - creates instance of MqttConsumer

| Param | Description | Required |
|---|:---:|---|
| host_or_dns_address | IP address of broker node or of the load balancing DNS | yes |
| is_DNS_address | is the first field DNS address or not | yes |
| topic | topic from which message should be received | no |
| consumerFunction | what to do with received message and full topic name | yes|
> Note: There are two constructors. One with all 4 arguments and another one with only required ones, so when subscribing you need to use method with newTopic argument.

> [Important note](https://www.hivemq.com/blog/mqtt-essentials-part-5-mqtt-topics-best-practices/): When a client subscribes to a topic, it can subscribe to the exact topic of a published message or 
it can use wildcards to subscribe to multiple topics simultaneously. ***A wildcard can only be used to subscribe to topics,
 not to publish a message***. There are two different kinds of wildcards: single-level (+) and multi-level (#).
 

By default:
- [Automatic reconnect](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#isAutomaticReconnect--) is set to true, in the event that the connection is lost, the client will
  attempt to reconnect to the server. It will initially wait 1 second before it attempts to
  reconnect, for each failed reconnection attempt, the delay will double until it is at 2 minutes
  at which point the delay will stay at 2 minutes. Also, connection
  timeout (connection establishment timeout in milliseconds; zero for infinite) is set to 0.
- The [clean session](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#isCleanSession--) is set to false and both the client and server will maintain state across
  restarts of client, server or connection. The server will treat a subscription as
  durable.
- [Keep-alive](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#setKeepAliveInterval-int-) interval (measured in seconds) defines the maximum time interval between
  messages sent or received. It enables the client to detect if the server is no longer available,
  without having to wait for the TCP/IP timeout. The client will ensure that at least one
  message travels across the network within each keep-alive period. In the absence of a
  data-related message during the time period, the client sends a very small "ping" message,
  which the server will acknowledge. The default value is 60 seconds, but we use 0 (we
  deactivate this mechanism because the subscriber cannot reconnect if network connection
  failure time is longer than keep-alive value). [This issue](https://github.com/eclipse/paho.mqtt.java/issues/576) with the paho library is known and it
  is a [milestone for the next version](https://github.com/eclipse/paho.mqtt.java/milestone/9).
- Port is automatically set to 1883. When the user enables SSL/TLS on the initiation of a
  connection, the port is changed to 8883.
- The paho client doesn't check the certificate expiration date on reconnect, so wrapper
  doesn’t renew the certificate!
- Difference to AMQP is that some parameters are set in the RabbitMQ configuration file. Like the
  name of exchange, queue time to live, and prefetch count.
  - {exchange, <<"presto.cloud">>},
  - {subscription_ttl, 86400000},
  - {prefetch, 10}
  
## Setters

`void setUsernameAndPassword(String username, String password)` - set username and password for connecting to the broker

`void setVhost(String vhost)` - set [virtual host](https://www.rabbitmq.com/vhosts.html) for connecting to the broker

`void useSSL(String vault, String token)` - set vault address and token for SSL/TLS connection
- sslSocketFactory is prepared with the certificate
  received from the Vault server (TLS v1.2) and added to the [connection options](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html). If it failed to create this
  factory it logs a warning and set connection without SSL/TLS.

`void turnOnSsl(boolean ssl)` - turn on/off usage of SSL/TLS certificates

`void setClientName(String clientName)` - set name of a paho client (it's used for generating queue name)

`void setDurable(boolean durable)` - durability of the queue (if true the queue will survive a broker restart)

`void setAutoDelete(boolean autoDelete)` - set auto delete option of the queue (if true queue that has had at least one consumer is deleted when last consumer unsubscribes)

`void loadBackedUpClientName()` - load backuped client name from file (used to restore state after consumer app goes down)

## Getters

`String getUsername()` - get current username

`String getPassword()` - get current password
- for now, it uses the default (guest) username and password.

`String getVhost()` - get current virtual host
- default vhost is "/", and probably there is no need for some other, but we enable the user to
  set vhost if later we decide to create specific vhost for specific users.
  
`boolean isSsl()` - check if connection is secure

`String getClientName()` - get current client name

`boolean isDurable()` - check if queue is durable

`boolean isAutoDelete()` - check if queue is auto-delete

`void backUpClientNameIfNeeded()` - backup client name to file (for later reconnection to same queue)  

## Subscribe methods

`void subscribe(String newTopic)` - subscribe to the broker

| Param | Description | Required | Default
|---|:---:|---|---|
| newTopic | topic name to which message should be sent to | no | topic set in constructor |

> Note: There are 2 versions of this method with and without newTopic argument

`void unsubscribe()` - unsubscribing from topic, disconnecting the client and closing the connection

Subscribe method checks if the newTopic is not null, checks if no other subscription already exists on this
instance, generates queue name, tries to create client, connects the client to broker with set options,
renews the certificate if needed, sets callback and starts consuming:
- Check if the topic name that user tries to connect with isn’t null. Log error message and return
IOException.
- Because of a lot of parameters for consuming and because the messages from other topics are
probably processed in a different way, the user needs to create one instance of the class for
every subscription to a different topic. So method checks if some connection already exists. Log
error message and return AlreadySubscribedException if this is the case.
- MqttConsumer instance tries to create new MQTT client with the **client name** and broker
URL. If the **client name** is null or an empty string, we generate it with ClientId **[“paho” +
nanoTime]**. RabbitMQ queue name depends on generated ClientId, and for each ClientId you can have
a queue per subscription Quality-of-Service level. It’s like ***“mqtt-subscription-” + client name + QoSLevel***.
User can also set some **client name**, and this functionality is used for recovery of consumer
application. If creating client fails it logs the error and exits.
- Set [callback](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttCallback.html) to the same class. Enable the application to be notified when asynchronous
events related to the client occur. Has four methods:
  - First is called when the connection to the server is successful.
  - Second is called when the connection to the server is lost. Log event.
  - Third is called when delivery for a message has been completed, and all
acknowledgements have been received. We don’t use this.
  - And fourth is called when a message arrives from the server. In this method, we
accept the message and topic name with a message consumer. If the application
needs to persist data, then it should ensure the data is persisted prior to returning
from this function, as after returning from this function, the message is considered
to have been delivered (auto acknowledge), and will not be reproducible. And
consumer logs the data.
- Try to connect to the broker with specified connection options. If the connection fails because
of an expired certificate, the certificate is renewed and new connection created. If it fails for
some other reason, it logs an error and returns SubscribingFailedException.
- [Subscribe](https://www.rabbitmq.com/mqtt.html#durability) to a defined topic with a selected level of QoS. The default state of QoS is 1.
  - Transient (QoS0) subscription uses non-durable, auto-delete queues that will be
deleted when the client disconnects.
  - Durable (QoS1) subscription uses durable queues. Whether the queues are
auto-deleted is controlled by the client's clean session flag. Clients with clean
sessions use auto-deleted queues, others use non-auto-deleted ones.
This can be changed with setDurable and setAutoDelete.
- If some exception occurred in these steps, connections will be closed, error logged and
SubscribingFailedException will be sent to the user.
- In the end, the user can unsubscribe from the broker. With unsubscribing user also
disconnects the client and closes the connection.

