package consumer;

import exceptions.NoHostException;
import org.eclipse.paho.client.mqttv3.*;
import exceptions.AlreadySubscribedException;
import exceptions.BackupException;
import exceptions.SubscribingFailedException;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.util.function.BiConsumer;

public class MqttConsumer extends RabbitMqConsumer {

    private MqttConnectOptions conOpt;
    private MqttClient client = null;

    private int qoS = 1;
    private Boolean cleanSession;

    public MqttConsumer(String address, boolean isDNS, String topic, BiConsumer<String, String> messageConsumer) {
        super(address, isDNS, topic, messageConsumer);
    }

    public MqttConsumer(String address, boolean isDNS, BiConsumer<String, String> messageConsumer) {
        super(address, isDNS, null, messageConsumer);
    }

    @Override
    public boolean isDurable() {
        return qoS == 1;
    }

    @Override
    public void setDurable(boolean durable) {
        if (durable) {
            qoS = 1;
        } else {
            qoS = 0;
        }
    }

    @Override
    public boolean isAutoDelete() {
        return qoS == 0 ? true : cleanSession;
    }

    @Override
    public void setAutoDelete(boolean autoDelete) {
        if (qoS == 1) {
            this.cleanSession = autoDelete;
            setUpConnection();
        } else {
            logger.info("[x] Queue is non durable, so it's also auto delete!");
        }
    }

    @Override
    protected void setUpConnection() {
        conOpt = new MqttConnectOptions();

        conOpt.setUserName(vhost + ":" + username);
        conOpt.setPassword(password.toCharArray());

        conOpt.setAutomaticReconnect(true);
        if (cleanSession == null)
            cleanSession = false;
        conOpt.setCleanSession(cleanSession);
        conOpt.setKeepAliveInterval(0);
        conOpt.setConnectionTimeout(0);

        mqttTrySSL();
    }

    private void mqttTrySSL() {
        protocolPart = "tcp://";
        if (isSsl()) {
            try {
                prepareSSL();
                protocolPart = "ssl://";
                conOpt.setSocketFactory(sslSocketFactory);
            } catch (Exception e) {
                logger.error("[x] Connection without SSL!");
                loggException(e);
            }
        }

        updateAllIps();
    }

    @Override
    protected void updateAllIps() {
        if (dns != null) {
            if (!ipList.isEmpty()) {
                String[] allIps = ipList.stream().map(i -> protocolPart + i).toArray(String[]::new);
                conOpt.setServerURIs(allIps);
            }
        } else if (host != null) {
            conOpt.setServerURIs(new String[]{protocolPart + host});
        }
    }

    @Override
    protected void tearDownConnection() {
        try {
            if (client != null && client.isConnected())
                client.disconnect();
        } catch (MqttException e) {
            logger.error("[x] Couldn't disconnect client!");
            loggException(e);
        }

        try {
            if (client != null)
                client.close();
        } catch (MqttException e) {
            logger.error("[x] Couldn't close client!");
            loggException(e);
        } finally {
            client = null;
        }

        logger.info("[x] Unsubscribe finished!");
    }

    @Override
    public void subscribe(String newTopic) throws AlreadySubscribedException, SubscribingFailedException, NoHostException, IOException {
        if (newTopic == null) {
            String errorMessage = "[x] Topic name can't be null";
            logger.error(errorMessage);
            throw new IOException(errorMessage);
        }

        if (client != null) {
            String errorMessage = "[x] You are already subscribe to this broker on " + topic +
                    "\n Create new instance of consumer class for subscribing to last topic.";
            logger.error(errorMessage);
            throw new AlreadySubscribedException(errorMessage);
        }

        topic = newTopic;
        loadBackedUpClientName();

        doBackup = clientName == null || clientName.isEmpty();

        if (doBackup)
            clientName = MqttClient.generateClientId();

        try {
            if (conOpt.getServerURIs() != null)
                client = new MqttClient(conOpt.getServerURIs()[0], clientName);
            else {
                String errorMessage = "[x] No host provided or DNS didn't return any record!";
                logger.error(errorMessage);
                throw new NoHostException(errorMessage);
            }
        } catch (MqttException e) {
            String errorMessage = "[x] Couldn't create client!";
            logger.error(errorMessage);
            loggException(e);
            throw new SubscribingFailedException(errorMessage);
        }

//        client.setManualAcks(!autoAck);
        client.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                logger.info("[x] Waiting for \"{}\" data on {}", topic, client.getCurrentServerURI());
            }

            @Override
            public void connectionLost(Throwable cause) {
                logger.info("[x] Connection unexpectedly lost");
                loggException((Exception) cause);
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                String message = new String(mqttMessage.getPayload());
                messageConsumer.accept(message, topic);
                logger.info("[x] Consume message: {}; from topic: {}.", message, topic);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
            }
        }); //for subscriber

        try {
            client.connect(conOpt);
            client.subscribe(topic);
            try {
                backUpClientNameIfNeeded();
            } catch (BackupException e) {
                logger.warn(e.getMessage());
            }
        } catch (MqttException e) {
            String errorMessage = "[x] Something went wrong!";
            logger.error(errorMessage);
            if (e.getCause() instanceof SSLHandshakeException && e.getCause().getMessage().contains("certificate_expired")) {
                mqttTrySSL();
                try {
                    client.connect(conOpt);
                } catch (MqttException e1) {
                    loggException(e1);
                    unsubscribe();
                    throw new SubscribingFailedException(errorMessage);
                }
            } else {
                loggException(e);
                unsubscribe();
                throw new SubscribingFailedException(errorMessage);
            }
        }
    }

    @Override
    public void unsubscribe() {
        try {
            if (client != null)
                client.unsubscribe(topic);
        } catch (MqttException e) {
            logger.error("[x] Couldn't cancel subscribing!");
            loggException(e);
        } finally {
            tearDownConnection();
        }
    }

//    @Override
//    public void ackMessage(long messageId, int qoS) {
//        if (!autoAck) {
//            try {
//                client.messageArrivedComplete((int) messageId, qoS);
//            } catch (MqttException e) {
//                loggException(e);
//            }
//        }
//    }

//    public static void main(String argv[]) {
//        String topic = "test/#";
//        java.util.function.BiConsumer<String, String> consumerFunction = MqttConsumer::doSomething;
//        MqttConsumer consumer = new MqttConsumer("3.120.91.124", true, consumerFunction);
//        consumer.useSSL("3.120.91.124", "s.pV8wvr4dkuq5ii476ddErT07");
//        boolean p = true;
//        while (p) {
//            try {
//                consumer.subscribe(topic);
//                p = false;
//            } catch (NoHostException | IOException | AlreadySubscribedException e1) {
//                e1.printStackTrace();
//                p = false;
//            } catch (SubscribingFailedException e) {
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e1) {
//                    e1.printStackTrace();
//                }
//            }
//        }
//    }
//
//    private static void doSomething(String message, String topic) {
//    }
}


